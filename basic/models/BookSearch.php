<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Book;

/**
 * BookSearch represents the model behind the search form of `app\models\Book`.
 */
class BookSearch extends Book
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'book_year', 'book_status', 'book_user'], 'integer'],
            [['book_name', 'book_image'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Book::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $query->andWhere(['book_status' => 0]);
        if (!Yii::$app->user->isGuest) {
            $user_id = Yii::$app->user->id;
            $query->orWhere(['book_status' => 1]);
            $query->orWhere(['and', ['book_status' => 2], ['book_user' => $user_id]]);

        }

        $this->load($params);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
//        $query->andFilterWhere([
//            'id' => $this->id,
//            'book_year' => $this->book_year,
//            'book_status' => $this->book_status,
//            'book_user' => $this->book_user,
//        ]);
        //$query->andFilterCompare('book_name', $this->book_name, 'ilike');
        $query->andFilterWhere(['like', 'book_name', $this->book_name]);
        return $dataProvider;
    }
}
