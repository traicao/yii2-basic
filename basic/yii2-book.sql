/*
SQLyog Community v12.4.3 (64 bit)
MySQL - 10.1.25-MariaDB : Database - bookyii2
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`bookyii2` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `bookyii2`;

/*Table structure for table `auth_assignment` */

DROP TABLE IF EXISTS `auth_assignment`;

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_assignment` */

insert  into `auth_assignment`(`item_name`,`user_id`,`created_at`) values 
('admin','6',1505447635),
('manager-book','1',1505447635),
('manager-book','3',1505448716);

/*Table structure for table `auth_item` */

DROP TABLE IF EXISTS `auth_item`;

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`),
  CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item` */

insert  into `auth_item`(`name`,`type`,`description`,`rule_name`,`data`,`created_at`,`updated_at`) values 
('admin',1,NULL,NULL,NULL,1505447634,1505447634),
('create-book',2,'Create a book',NULL,NULL,1505447634,1505447634),
('delete-book',2,'Delete a book',NULL,NULL,1505447634,1505447634),
('index-book',2,'View list book',NULL,NULL,1505447634,1505447634),
('manager-book',1,NULL,NULL,NULL,1505447634,1505447634),
('update-book',2,'Update a book',NULL,NULL,1505447634,1505447634),
('view-book',2,'View detail a book',NULL,NULL,1505447634,1505447634);

/*Table structure for table `auth_item_child` */

DROP TABLE IF EXISTS `auth_item_child`;

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_item_child` */

insert  into `auth_item_child`(`parent`,`child`) values 
('admin','create-book'),
('admin','delete-book'),
('admin','manager-book'),
('admin','update-book'),
('manager-book','index-book'),
('manager-book','view-book');

/*Table structure for table `auth_rule` */

DROP TABLE IF EXISTS `auth_rule`;

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*Data for the table `auth_rule` */

/*Table structure for table `book` */

DROP TABLE IF EXISTS `book`;

CREATE TABLE `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_name` varchar(250) NOT NULL,
  `book_image` varchar(250) DEFAULT NULL,
  `book_year` int(11) NOT NULL,
  `book_status` int(11) NOT NULL,
  `book_user` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `book` */

insert  into `book`(`id`,`book_name`,`book_image`,`book_year`,`book_status`,`book_user`) values 
(1,'a','abc.jpg',1990,0,1),
(2,'b','abc.jpg',1998,2,2),
(3,'c','abc.jpg',1990,1,1),
(4,'d','abc.jpg',1990,1,1),
(5,'e','abc.jpg',1990,2,1),
(6,'m','aa.jpg',1990,1,1);

/*Table structure for table `migration` */

DROP TABLE IF EXISTS `migration`;

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `migration` */

insert  into `migration`(`version`,`apply_time`) values 
('m000000_000000_base',1505446634),
('m140506_102106_rbac_init',1505447045);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(250) NOT NULL,
  `password` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `authKey` varchar(250) NOT NULL,
  `password_reset_token` varchar(250) DEFAULT NULL,
  `user_image` varchar(250) DEFAULT 'default.jpg',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

insert  into `user`(`id`,`username`,`password`,`email`,`authKey`,`password_reset_token`,`user_image`) values 
(1,'crtrai','$2y$13$wdwZ3pP.gdEaukzSqCzoO.eusC4nhQXlZ0xqcDRjpipZB/frZrZ3G','crtrai@gmail.com','TGEazYi9LSDJhQ91syeIzBxInYW-grRW',NULL,'default.jpg'),
(2,'crtrai1','$2y$13$ffAKhJXSHmSAfRTczSJgUuRPDkHW4LvS.ZtOenyC4ojRC8MKmCqN.','crtrai1@gmail.com','Rm2OdtJS1DtzGPhSN9f_6C-0qXlQgKUp',NULL,'default.jpg'),
(3,'test','$2y$13$ugdQMf.zSmJtq8AHLOTmcuaxvm4WFyM60pTnHcctNXoSb22r0lQ/O','caongoctra@gmail.com','fU_HfJ3K8e14fBkr_EhJ5LlXvu5M2_ac',NULL,'default.jpg'),
(4,'test1','$2y$13$MomgBPOQ83a.TboCVFcrcupKdLrJVm.s2O3ZRJot4tUyzLafe7O8K','caongoctra1@gmail.com','LUVc2mHif8kuXX6n80BeyiKXuXAVn4tA',NULL,'default.jpg'),
(5,'test2','$2y$13$2pmDJ8XaAjT/iPu2Ij5Lqe7f4ik9nQxG7Tg3RP72YxvSWycfY0XM.','caongoctra12@gmail.com','_8UWkTXGSMKgF4n_M8NvPt4BQdIficAw',NULL,'default.jpg'),
(6,'test3','$2y$13$BcPResK7GrKGIA9ZMRNV4uA50vzpDZBIqNyaOwUF3uTSZ70FNiHAe','congoctra12@gmail.com','HSAndbCx_WvpG0eoOFDndrqjosEqjqws',NULL,'default.jpg');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
