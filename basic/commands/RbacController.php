<?php
namespace app\commands;

use yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        // add "createBook" permission
        $createBook = $auth->createPermission('create-book');
        $createBook->description = 'Create a book';
        $auth->add($createBook);

        // add "updateBook" permission
        $updateBook = $auth->createPermission('update-book');
        $updateBook->description = 'Update a book';
        $auth->add($updateBook);

        // add "viewBook" permission
        $viewBook = $auth->createPermission('view-book');
        $viewBook->description = 'View detail a book';
        $auth->add($viewBook);

        // add "indexBook" permission
        $indexBook = $auth->createPermission('index-book');
        $indexBook->description = 'View list book';
        $auth->add($indexBook);

        // add "deleteBook" permission
        $deleteBook = $auth->createPermission('delete-book');
        $deleteBook->description = 'Delete a book';
        $auth->add($deleteBook);

        //add "manager-book" role
        $bookManager= $auth->createRole('manager-book');
        $auth->add($bookManager);

        //add "admin" role
        $admin= $auth->createRole('admin');
        $auth->add($admin);

        $auth->addChild($bookManager,$indexBook);
        $auth->addChild($bookManager,$viewBook);

        //add child
        $auth->addChild($admin,$bookManager);
        $auth->addChild($admin,$updateBook);
        $auth->addChild($admin,$createBook);
        $auth->addChild($admin,$deleteBook);

        //add assign to user
        $auth->assign($bookManager,1);
        $auth->assign($admin,2);


    }
}
