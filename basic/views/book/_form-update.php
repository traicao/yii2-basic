<?php

use yii\helpers\Html;
use app\models\Book;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Book */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="book-form">
    <div class="row">
        <div class="col-md-4">
            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

            <?= $form->field($model, 'book_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'book_image_new')->fileInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'book_year')->textInput() ?>

            <?= $form->field($model,'book_status')->dropDownList(Book::changeBookStatus()) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
